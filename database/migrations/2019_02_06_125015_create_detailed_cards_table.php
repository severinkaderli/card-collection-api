<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailedCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailed_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('number_of_copies');
            $table->string('image')->nullable();
            $table->string('setnumber');
            $table->string('variant')->nullable();
            $table->decimal('value')->default(0);
            $table->integer('state')->default(0);
            $table->unsignedInteger('rarity_id')->nullable();
            $table->unsignedInteger('set_id');
            $table->unsignedInteger('card_id');
            $table->unsignedInteger('language_id');

            $table->foreign('rarity_id')->references('id')->on('rarities');
            $table->foreign('set_id')->references('id')->on('sets');
            $table->foreign('card_id')->references('id')->on('cards');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailed_cards');
    }
}
