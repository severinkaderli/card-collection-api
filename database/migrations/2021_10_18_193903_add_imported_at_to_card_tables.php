<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportedAtToCardTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->timestamp('imported_at')->nullable();
        });

        Schema::table('detailed_cards', function (Blueprint $table) {
            $table->timestamp('imported_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn('imported_at');
        });

        Schema::table('detailed_cards', function (Blueprint $table) {
            $table->dropColumn('imported_at');
        });
    }
}
