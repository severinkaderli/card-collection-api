<?php

use Illuminate\Database\Seeder;

class SetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sets = [
            [
                'id' => 1,
                'name' => 'Base Set',
                'code' => 'DM-01',
                'game_id' => 3
            ],
            [
                'id' => 2,
                'name' => 'Evo-Crushinators of Doom',
                'code' => 'DM-02',
                'game_id' => 3
            ],
            [
                'id' => 3,
                'name' => 'Rampage of the Super Warriors',
                'code' => 'DM-03',
                'game_id' => 3
            ],
            [
                'id' => 4,
                'name' => 'Shadowclash of Blinding Night',
                'code' => 'DM-04',
                'game_id' => 3
            ],
            [
                'id' => 5,
                'name' => 'Survivors of the Megapocalypse',
                'code' => 'DM-05',
                'game_id' => 3
            ],
            [
                'id' => 6,
                'name' => 'Stomp-A-Trons of Invincible Wrath',
                'code' => 'DM-06',
                'game_id' => 3
            ],
            [
                'id' => 7,
                'name' => 'Thundercharge of Ultra Destruction',
                'code' => 'DM-07',
                'game_id' => 3
            ],
            [
                'id' => 8,
                'name' => 'Epic Dragons of Hyperchaos',
                'code' => 'DM-08',
                'game_id' => 3
            ],
            [
                'id' => 9,
                'name' => 'Fatal Brood of Infinite Ruin',
                'code' => 'DM-09',
                'game_id' => 3
            ],
            [
                'id' => 10,
                'name' => 'Shockwaves of the Shattered Rainbow',
                'code' => 'DM-10',
                'game_id' => 3
            ],
            [
                'id' => 11,
                'name' => 'Blast-O-Splosion of Gigantic Rage',
                'code' => 'DM-11',
                'game_id' => 3
            ],
            [
                'id' => 12,
                'name' => 'Thrash of the Hybrid Megacreatures',
                'code' => 'DM-12',
                'game_id' => 3
            ],
        ];

        foreach($sets as $set) {
            if(!DB::table('sets')->where('id', $set['id'])->exists()) {
                DB::table('sets')->insert($set);
            }
        }
    }
}
