<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            [
                'id' => 1,
                'name' => 'German',
                'code' => 'de',
            ],
            [
                'id' => 2,
                'name' => 'English',
                'code' => 'en',
            ],
            [
                'id' => 3,
                'name' => 'Japanese',
                'code' => 'jp',
            ],
            [
                'id' => 4,
                'name' => 'Italian',
                'code' => 'it',
            ]
        ];

        foreach($languages as $language) {
            if(!DB::table('languages')->where('id', $language['id'])->exists()) {
                DB::table('languages')->insert($language);
            }
        }
    }
}
