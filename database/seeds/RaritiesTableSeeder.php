<?php

use Illuminate\Database\Seeder;

class RaritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rarities = [
            [
                'id' => 1,
                'name' => 'Common',
                'abbreviation' => 'C',
                'game_id' => 1
            ],
            [
                'id' => 2,
                'name' => 'Uncommon',
                'abbreviation' => 'U',
                'game_id' => 1
            ],
            [
                'id' => 3,
                'name' => 'Rare',
                'abbreviation' => 'R',
                'game_id' => 1
            ],
            [
                'id' => 4,
                'name' => 'Mythic',
                'abbreviation' => 'M',
                'game_id' => 1
            ],
            [
                'id' => 5,
                'name' => 'Common',
                'abbreviation' => 'C',
                'game_id' => 2
            ],
            [
                'id' => 6,
                'name' => 'Rare',
                'abbreviation' => 'R',
                'game_id' => 2
            ],
            [
                'id' => 7,
                'name' => 'Super Rare',
                'abbreviation' => 'SR',
                'game_id' => 2
            ],
            [
                'id' => 8,
                'name' => 'Ultra Rare',
                'abbreviation' => 'UR',
                'game_id' => 2
            ],
            [
                'id' => 9,
                'name' => 'Ultimate Rare',
                'abbreviation' => 'UtR',
                'game_id' => 2
            ],
            [
                'id' => 10,
                'name' => 'Secret Rare',
                'abbreviation' => 'ScR',
                'game_id' => 2
            ],
            [
                'id' => 11,
                'name' => 'Ghost Rare',
                'abbreviation' => 'GR',
                'game_id' => 2
            ],
            [
                'id' => 12,
                'name' => 'Common',
                'abbreviation' => 'C',
                'game_id' => 3
            ],
            [
                'id' => 13,
                'name' => 'Uncommon',
                'abbreviation' => 'U',
                'game_id' => 3
            ],
            [
                'id' => 14,
                'name' => 'Rare',
                'abbreviation' => 'R',
                'game_id' => 3
            ],
            [
                'id' => 15,
                'name' => 'Very Rare',
                'abbreviation' => 'VR',
                'game_id' => 3
            ],
            [
                'id' => 16,
                'name' => 'Super Rare',
                'abbreviation' => 'SR',
                'game_id' => 3
            ],
            [
                'id' => 17,
                'name' => 'Common',
                'abbreviation' => 'C',
                'game_id' => 4
            ],
            [
                'id' => 18,
                'name' => 'Uncommon',
                'abbreviation' => 'U',
                'game_id' => 4
            ],
            [
                'id' => 19,
                'name' => 'Rare',
                'abbreviation' => 'R',
                'game_id' => 4
            ],
            [
                'id' => 20,
                'name' => 'Prismatic Secret Rare',
                'abbreviation' => 'PScR',
                'game_id' => 2
            ],
            [
                'id' => 21,
                'name' => 'Ultra Rare (Green)',
                'abbreviation' => 'UR (Green)',
                'game_id' => 2
            ],
            [
                'id' => 22,
                'name' => 'Ultra Rare (Blue)',
                'abbreviation' => 'UR (Blue)',
                'game_id' => 2
            ],
            [
                'id' => 23,
                'name' => 'Ultra Rare (Purple)',
                'abbreviation' => 'UR (Purple)',
                'game_id' => 2
            ],
        ];

        foreach($rarities as $rarity) {
            if(!DB::table('rarities')->where('id', $rarity['id'])->exists()) {
                DB::table('rarities')->insert($rarity);
            }
        }
    }
}
