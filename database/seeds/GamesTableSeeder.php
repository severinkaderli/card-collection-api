<?php

use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = [
            [
                'id' => 1,
                'name' => 'Magic the Gathering',
                'abbreviation' => 'MTG',
            ],
            [
                'id' => 2,
                'name' => 'Yu-Gi-Oh!',
                'abbreviation' => 'YGO',
            ],
            [
                'id' => 3,
                'name' => 'Duel Masters',
                'abbreviation' => 'DM',
            ],
            [
                'id' => 4,
                'name' => 'Pokémon Trading Card Game',
                'abbreviation' => 'PKMN',
            ],
            [
                'id' => 5,
                'name' => 'Digimon Card Game',
                'abbreviation' => 'DCG',
            ],
        ];

        foreach($games as $game) {
            if(!DB::table('games')->where('id', $game['id'])->exists()) {
                DB::table('games')->insert($game);
            }
        }
    }
}
