#!/bin/sh

cd /var/www/html

# Caching
php artisan optimize
php artisan cache:clear
php artisan route:cache
php artisan config:cache

# Start php-fpm
cron && apache2-foreground
