FROM php:8.3-apache

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN apt-get update && apt-get install -y \
    libpq-dev \
    libonig-dev \
    zlib1g-dev \
    libpng-dev \
    libzip-dev \
    zip \
    unzip \
    cron
    
# Enable cron jobs
COPY ./crontab /etc/cron.d/crontab
RUN chmod 0644 /etc/cron.d/crontab
RUN /usr/bin/crontab /etc/cron.d/crontab
RUN touch /var/log/cron.log

# Install PHP extensions
RUN docker-php-ext-install zip pgsql pdo_pgsql pdo_mysql mbstring exif pcntl bcmath gd

# Enable modules
RUN a2enmod rewrite

COPY . /var/www/html/

ADD start.sh /usr/local/bin/start.sh
RUN chmod 777 /usr/local/bin/start.sh

RUN composer install --no-dev --no-scripts

RUN sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!/var/www/html/public/!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

CMD ["/usr/local/bin/start.sh"]
