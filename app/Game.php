<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    /**
     * Hidden fields.
     *
     * @var array
     */
    protected $hidden = [
        "created_at",
        "updated_at",
    ];
}
