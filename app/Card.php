<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
     * Hidden fields.
     *
     * @var array
     */
    protected $hidden = [
        "created_at",
        "updated_at",
        "game_id"
    ];

    protected $appends = [
        "number_of_copies",
        "value"
    ];

    protected $fillable = ["name", "game_id"];

    public function game() {
        return $this->belongsTo('App\Game');
    }

    public function cards() {
        return $this->hasMany('App\DetailedCard');
    }

    public function getNumberOfCopiesAttribute() {
        $count = 0;
        foreach($this->cards as $card) {
            $count += $card -> number_of_copies;
        }
        return $count;
    }

    public function getValueAttribute() {
        $value = 0;
        foreach($this->cards as $card) {
            $value += $card -> value * $card -> number_of_copies;
        }
        return $value;
    }
}
