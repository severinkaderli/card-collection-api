<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailedCard extends Model
{
    const STATE_FOIL = 1;
    const STATE_PROMO = 2;
    const STATE_REVERSE_FOIL = 4;

    /**
     * Hidden fields.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'set_id',
        'card_id',
        'rarity_id',
        'language_id',
    ];

    protected $appends = ['state_array'];

    protected $fillable = ['number_of_copies', 'setnumber', 'variant', 'rarity_id', 'set_id', 'card_id', 'language_id', 'state'];

    public function rarity()
    {
        return $this->belongsTo('App\Rarity', 'rarity_id', 'id', __FUNCTION__);
    }

    public function set()
    {
        return $this->belongsTo('App\Set', 'set_id', 'id', __FUNCTION__);
    }

    public function card()
    {
        return $this->belongsTo('App\Card', 'card_id', 'id', __FUNCTION__);
    }

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'id', __FUNCTION__);
    }

    public function getTotalValueAttribute() {
        return $this->value * $this->number_of_copies;
    }
}
