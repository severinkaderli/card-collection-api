<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sets:fetch:mtg')->weekly();
        $schedule->command('sets:fetch:ygo')->weekly();
        $schedule->command('sets:fetch:pkmn')->weekly();

        $schedule->command('update:mtg')->everyFifteenMinutes()->appendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('update:ygo')->everyFifteenMinutes()->appendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('update:dm')->everyFifteenMinutes()->appendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('update:pkmn')->everyFifteenMinutes()->appendOutputTo(storage_path('logs/cron.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
    }
}
