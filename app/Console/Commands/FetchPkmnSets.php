<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use Illuminate\Console\Command;

class FetchPkmnSets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sets:fetch:pkmn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new GuzzleHttp\Client();
        $res = $client->get('https://api.pokemontcg.io/v1/sets');
        $sets = json_decode($res->getBody())->sets;

        foreach($sets as $set) {
            $code = $set->code;

            $whereClause = [
                ["code", "=", strtoupper($code)],
                ["game_id", "=", "4"]
            ];
            if(!Set::where($whereClause)->exists()) {
                $newSet = new Set;
                $newSet->name = $set->name;
                $newSet->code = strtoupper($code);
                $newSet->game_id = 4;
                $newSet->save();
            }
        }
    }
}