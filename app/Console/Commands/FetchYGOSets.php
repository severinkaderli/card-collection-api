<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use Illuminate\Console\Command;

class FetchYGOSets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sets:fetch:ygo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Yu-Gi-Oh! and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new GuzzleHttp\Client();
        $res = $client->get('https://db.ygoprodeck.com/api/v7/cardsets.php');
        $sets = json_decode($res->getBody());

        foreach($sets as $set) {
            $setCode = $set->set_code;
            $setName = $set->set_name;

            $whereClause = [
                ["code", "=", strtoupper($setCode)],
                ["game_id", "=", "2"]
            ];

            if(!Set::where($whereClause)->exists()) {
                $newSet = new Set;
                $newSet->name = $setName;
                $newSet->code = strtoupper($setCode);
                $newSet->game_id = 2;
                $newSet->save();
            }
        }
    }
}