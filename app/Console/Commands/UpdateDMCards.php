<?php

namespace App\Console\Commands;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use App\Card;
use App\DetailedCard;
use App\Game;
use App\Language;
use Illuminate\Console\Command;

class UpdateDMCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:dm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getSetNumber($card) {
        $setParts = explode("-", $card -> set -> code);
        return intval($setParts[1]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cards = DetailedCard::whereHas("card", function($query) {
            $query -> where('game_id', '=', 3);
        })->orderBy('updated_at', 'asc')->limit(100)->get();
        $client = new GuzzleHttp\Client();

        foreach($cards as $card) {
            echo $card -> card -> name . "\n";
            $setNumber = $this->getSetNumber($card);
            $cardNumber = $card -> setnumber;

            $card -> image = "http://img.ccgdb.com/duelmasters/cards/" . $setNumber . "/" . $cardNumber . ".jpg";
            $card -> save();
            $card -> touch();
        }
    }
}