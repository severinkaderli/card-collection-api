<?php

namespace App\Console\Commands;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use App\Card;
use App\DetailedCard;
use App\Game;
use App\Language;
use Illuminate\Console\Command;

class UpdatePkmnCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:pkmn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cards = DetailedCard::whereHas("card", function($query) {
            $query -> where('game_id', '=', 4);
        })->orderBy('updated_at', 'asc')->limit(100)->get();
        $client = new GuzzleHttp\Client();

        foreach($cards as $card) {
            echo $card -> card -> name . "\n";
            $result = $client -> get("https://api.pokemontcg.io/v1/cards?id=" . strtolower($card->set->code) . "-" . $card -> setnumber);
            $data = json_decode($result->getBody());

            $card -> image = $data->cards[0] -> imageUrl;
            $card -> save();
            $card -> touch();
        }
    }
}