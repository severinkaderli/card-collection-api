<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use App\Card;
use App\DetailedCard;
use App\Game;
use App\Language;
use Illuminate\Console\Command;

class InsertDMCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:dm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return;
        $spreadsheetUrl = "https://sheets.googleapis.com/v4/spreadsheets/1kHONkG9mw4KKvGEgsgORIrWzh_2eud2pmUo26OAggGM/values/'Duel Masters'!A2:G1000?key=";
        $client = new GuzzleHttp\Client();
        $res = $client->get($spreadsheetUrl);
        $cards = json_decode($res->getBody())->values;

        foreach($cards as $c) {

            $card = Card::where('name', $c[0])->first();
            if(is_null($card)) {
                $card = new Card();
                $card -> name = $c[0];
                $card -> game_id = 3;
                $card -> save();
            }

            // Insert detailed card
            $detailedCard = new DetailedCard();
            $detailedCard->setnumber = $c[2];
            $detailedCard->number_of_copies = $c[3];
            $detailedCard->card_id = $card -> id;

            $state = 0;
            if(!empty($c[6]) && $c[6] == "🗙") {
                $state = DetailedCard::STATE_PROMO;
            }
            $detailedCard->state =  $state;

            $whereClause = [
                ["code", "=", $c[1]],
                ["game_id", "=", "3"]
            ];
            $set = Set::where($whereClause)->first();
            $detailedCard->set_id = $set ->id;

            $langCode = 'en';
            if(!empty($c[5])) {
                $langCode = trim($c[5]);
            }

            $language = Language::where("code", "=", $langCode)->first();
            $detailedCard->language_id = $language->id;
            $detailedCard->save();

        }
    }
}
