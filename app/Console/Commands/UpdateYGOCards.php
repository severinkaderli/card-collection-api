<?php

namespace App\Console\Commands;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp;
use App\DetailedCard;
use Illuminate\Console\Command;
use Carbon\Carbon;

class UpdateYGOCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:ygo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getAPIUrl($card, $langCode)
    {
        $setNumber = str_pad($card->setnumber, 3, '0', STR_PAD_LEFT);
        $setCode = $card->set->code;

        if($setCode == "SRL") {
            $setCode = "MRL";
        }

        return 'https://db.ygoprodeck.com/api/v7/cardsetsinfo.php?setcode='.$setCode.'-'.strtoupper($langCode).$setNumber;
    }

    private function apiRequest($card, $langKey) {
        $client = new GuzzleHttp\Client();
        echo "\t" . ' - ' . $this->getAPIUrl($card, $langKey) . PHP_EOL;

        try {
            $result = $client->get($this->getAPIUrl($card, $langKey));
        } catch (ClientException $exception) {
            return null;
        }

        return json_decode($result->getBody());
    }

    protected function getType($type, $race)
    {
        return match($type) {
            // Normal
            "Normal Monster" => "${race} / Normal",
            "Normal Tuner Monster" => "${race} / Tuner / Normal",

            // Effect
            "Effect Monster" => "${race} / Effect",
            "Flip Effect Monster" => "${race} / Flip / Effect",
            "Flip Tuner Effect Monster" => "${race} / Flip / Tuner / Normal",
            "Gemini Monster" => "${race} / Gemini / Effect",
            "Tuner Monster" => "${race} / Tuner / Normal",
            "Union Effect Monster" => "${race} / Union / Effect",
            "Toon Monster" => "${race} / Toon / Effect",

            // Ritual
            "Spirit Monster" => "${race} / Spirit / Effect",
            "Ritual Monster" => "${race} / Ritual",
            "Ritual Effect Monster" => "${race} / Ritual / Effect",

            // Pendulum
            "Pendulum Normal Monster" => "${race} / Pendulum",
            "Pendulum Effect Monster" => "${race} / Pendulum / Effect",
            "Pendulum Flip Effect Monster" => "${race} / Pendulum / Flip / Effect",
            "Pendulum Tuner Effect Monster" => "${race} / Tuner / Pendulum / Effect",

            // Fusion
            "Fusion Monster" => "${race} / Fusion",
            "Pendulum Effect Fusion Monster" => "${race} / Fusion / Pendulum / Effect",

            // Synchro
            "Synchro Monster" => "${race} / Synchro",
            "Synchro Tuner Monster" => "${race} / Synchro / Tuner / Effect",
            "Synchro Pendulum Effect Monster" => "${race} / Synchro / Pendulum / Effect",

            // Xyz
            "XYZ Monster" => "${race} / Xyz / Effect",
            "XYZ Pendulum Effect Monster" => "${race} / Xyz / Pendulum / Effect",

            // Link
            "Link Monster" => "${race} / Link / Effect",

            // Spell and Traps
            "Spell Card" => "Spell Card / ${race}",
            "Trap Card" => "Trap Card / ${race}",

            // Speed Duel
            "Skill Card" => "${race} / Skill",
            default => throw new Exception('Unknown type: ' . $type),
        };
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new GuzzleHttp\Client();
        $detailedCards = DetailedCard::whereHas('card', function ($query) {
            $query->where('game_id', '=', 2);
        })->orderBy('updated_at', 'asc')->limit(250)->get();


        foreach ($detailedCards as $detailedCard) {
            echo $detailedCard->card->name. PHP_EOL;

            if(in_array($detailedCard->set->code, ['LOB', 'MRD', 'SRL', 'PSV', 'MRL', 'LON', 'SDP', 'SYE', 'SDY', 'SDK', 'SDJ', 'SKE'])) {
                $langKey = 'E';
            } else {
                $langKey = 'EN';
            }


            $data = $this->apiRequest($detailedCard, $langKey);

            while (is_null($data) || !isset($data) || \property_exists($data, 'error')) {
                if (strtoupper($langKey) == 'E') {
                    $langKey = 'EN';
                    $data = $this->apiRequest($detailedCard, $langKey);
                    continue;
                }

                if (strtoupper($langKey) == 'EN') {
                    $langKey = '';
                    $data = $this->apiRequest($detailedCard, $langKey);
                    continue;
                }


                if (empty($langKey)) {
                    echo 'Nothing found. Skipping!' . PHP_EOL;
                    $detailedCard->touch();
                    continue 2;
                }
            }

            $cardData = json_decode($client->get('https://db.ygoprodeck.com/api/v7/cardinfo.php?id=' . $data->id ) ->getBody())->data[0];
            $detailedCard->value = $cardData->card_prices[0]->cardmarket_price;
            $detailedCard->image = $cardData->card_images[0]->image_url;
            //$detailedCard->imported_at = Carbon::now();
            $detailedCard->save();

            $metadata = [
                'type' => $this->getType($cardData->type, $cardData->race),
                'description' => $cardData->desc,
            ];

            $detailedCard->card->metadata = json_encode($metadata);
            //$detailedCard->card->imported_at = Carbon::now();
            $detailedCard->card->save();
        }
    }
}