<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use Illuminate\Console\Command;

class FetchMtgSets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sets:fetch:mtg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new GuzzleHttp\Client();
        $res = $client->get('https://api.scryfall.com/sets');
        $sets = json_decode($res->getBody())->data;
        foreach($sets as $set) {

            $whereClause = [
                ["code", "=", strtoupper($set->code)],
                ["game_id", "=", "1"]
            ];
            if(!Set::where($whereClause)->exists()) {
                $newSet = new Set;
                $newSet->name = $set->name;
                $newSet->code = strtoupper($set->code);
                $newSet->game_id = 1;
                $newSet->save();
            }
        }
    }
}