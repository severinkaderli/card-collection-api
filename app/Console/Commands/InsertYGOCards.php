<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use App\Card;
use App\DetailedCard;
use App\Game;
use App\Rarity;
use App\Language;
use Illuminate\Console\Command;

class InsertYGOCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:ygo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return;
        $spreadsheetUrl = "https://sheets.googleapis.com/v4/spreadsheets/1kHONkG9mw4KKvGEgsgORIrWzh_2eud2pmUo26OAggGM/values/'Yu-Gi-Oh!'!A2:F2000?key=";
        $client = new GuzzleHttp\Client();
        $res = $client->get($spreadsheetUrl);
        $cards = json_decode($res->getBody())->values;

        foreach($cards as $c) {
            $card = Card::where('name', $c[0])->first();
            if(is_null($card)) {
                $card = new Card();
                $card -> name = $c[0];
                $card -> game_id = 2;
                $card -> save();
            }

            // Get set code and set number
            $setParts = explode("-", $c[1]);
            $setCode = $setParts[0];
            $matches = [];
            preg_match('/(DE|G|EN|E)?(.*)/i', $setParts[1], $matches);
            $langCode = isset($matches[1]) ? strtolower($matches[1]) : 'en';
            $setNumber = isset($matches[2]) ? $matches[2] : $matches[1];

            // Insert detailed card
            $detailedCard = new DetailedCard();
            $detailedCard->setnumber = ltrim($setNumber, "0");
            if(isset($c[3])) {
                $detailedCard->variant = $c[3];
            }
            $detailedCard->number_of_copies = $c[4];
            $detailedCard->card_id = $card -> id;

            $rarityWhereClause = [
                ["name", "=", $c[2]],
                ["game_id", "=", "2"]
            ];
            $rarity = Rarity::where($rarityWhereClause)->first();
            $detailedCard->rarity_id = $rarity ->id;

            // Set
            $setWhereClause = [
                ["code", "=", $setCode],
                ["game_id", "=", "2"]
            ];
            $set = Set::where($setWhereClause)->first();
            if(is_null($set)) {
                $set = new Set();
                $set -> name = "";
                $set -> code = $setCode;
                $set -> game_id = 2;
                $set -> save();
            }
            $detailedCard->set_id = $set ->id;

            if($langCode == 'g') {
                $langCode = 'de';
            }

            if($langCode == "e" || empty($langCode)) {
                $langCode = 'en';
            }

            $language = Language::where("code", "=", $langCode)->first();
            $detailedCard->language_id = $language->id;
            $detailedCard->save();

        }
    }
}
