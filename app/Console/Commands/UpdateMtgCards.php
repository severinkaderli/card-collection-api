<?php

namespace App\Console\Commands;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use GuzzleHttp;
use App\Set;
use App\Card;
use App\DetailedCard;
use App\Game;
use App\Language;
use Illuminate\Console\Command;

class UpdateMtgCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:mtg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches sets from Magic the Gathering and updates the database with new sets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function getApiUrl($setCode, $setNumber, $langCode = '') {
        $apiUrl = "https://api.scryfall.com/cards/";
        $apiUrl .= strtolower($setCode) . "/";
        $apiUrl .= $setNumber ;
        if(isset($langCode)) {
            $apiUrl .= "/" . strtolower($langCode);
        }

        return $apiUrl;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cards = DetailedCard::whereHas("card", function($query) {
            $query -> where('game_id', '=', 1);
        })->orderBy('updated_at', 'asc')->limit(100)->get();
        $client = new GuzzleHttp\Client();

        foreach($cards as $card) {
            echo $card -> card -> name . "\n";

            $apiUrl =  $this -> getApiUrl($card -> set -> code, $card -> setnumber, $card -> language -> code);

            try {
                $result = $client -> get($apiUrl);
            } catch(ClientException $exception) {
                $apiUrl =  $this -> getApiUrl($card -> set -> code, $card -> setnumber);

                try {
                    $result = $client -> get($apiUrl);
                } catch(ClientException $exception) {
                    $card->touch();
                    continue;
                }

            }

            $data = json_decode($result->getBody());
            if(property_exists($data, 'image_uris')) {
                $card -> image = $data -> image_uris -> large;
            } else if(property_exists($data, 'card_faces')) {
                $card -> image = $data -> card_faces[0] -> image_uris -> large;
            }

            $card -> value = isset($data -> prices -> eur) ? $data -> prices -> eur : 0;
            $card -> save();
            $card -> touch();
        }
    }
}
