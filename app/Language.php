<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * Hidden fields.
     *
     * @var array
     */
    protected $hidden = [
        "created_at",
        "updated_at"
    ];
}
