<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Language as LanguageResource;
use App\Http\Resources\Set as SetResource;

class DetailedCard extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this -> id,
            'language' => new LanguageResource($this->language),
            'set' => new SetResource($this -> set)
        ];
    }
}