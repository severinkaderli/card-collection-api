<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SetParentCard as SetParentCardResource;

class SetCard extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'number_of_copies' => $this->number_of_copies,
            'setnumber' => $this->setnumber,
            'card' => new SetParentCardResource($this->card),
        ];
    }
}
