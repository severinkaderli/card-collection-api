<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Language as LanguageResource;
use App\Http\Resources\Rarity as RarityResource;
use App\Http\Resources\FlatSet as FlatSetResource;

class FlatDetailedCard extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number_of_copies' => $this->number_of_copies,
            'image' => $this->image,
            'setnumber' => $this->setnumber,
            'variant' => $this->variant,
            'rarity' => new RarityResource($this->rarity),
            'language' => new LanguageResource($this->language),
            'set' => new FlatSetResource($this->set),
            'state' => $this->state,
            'singleValue' => floatval($this->value),
            'totalValue' => $this->totalValue,
        ];
    }
}
