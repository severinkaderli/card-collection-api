<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Type as TypeResource;
use App\Http\Resources\FlatDetailedCard as FlatDetailedCardResource;

class Card extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'number_of_copies' => $this->numberOfCopies,
            'cards' => FlatDetailedCardResource::collection($this->cards),
            'metadata' => json_decode($this->metadata),
        ];
    }
}
