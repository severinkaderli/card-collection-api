<?php

namespace App\Http\Controllers;

use App\DetailedCard;
use Illuminate\Http\Request;

class DetailedCardController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detailedCard = DetailedCard::create($request->all());

        if(!$detailedCard->save()) {
            throw new ApiException(500, "UNKNOWN_ERROR");
        }

        return response()->json([]);
    }

   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetailedCard  $detailedCard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailedCard $detailedCard)
    {
        $detailedCard -> fill($request -> all());

        if(!$detailedCard->save()) {
            throw new ApiException(500, "UNKNOWN_ERROR");
        }

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetailedCard  $detailedCard
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailedCard $detailedCard)
    {
        if(!$detailedCard->delete()) {
            throw new ApiException(500, "UNKNOWN_ERROR");
        }

        return response()->json([]);
    }
}
