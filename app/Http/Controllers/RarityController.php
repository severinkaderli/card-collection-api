<?php

namespace App\Http\Controllers;

use App\Rarity;
use Illuminate\Http\Request;

class RarityController extends Controller
{
    protected $with = [
        'game'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gameId = $request->input('game');
        if(isset($gameId)) {
            return Rarity::where('game_id', $gameId)->get();
        }

        return Rarity::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rarity  $rarity
     * @return \Illuminate\Http\Response
     */
    public function show(Rarity $rarity)
    {
        return $rarity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rarity  $rarity
     * @return \Illuminate\Http\Response
     */
    public function edit(Rarity $rarity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rarity  $rarity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rarity $rarity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rarity  $rarity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rarity $rarity)
    {
        //
    }
}
