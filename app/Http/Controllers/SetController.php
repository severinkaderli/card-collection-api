<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Set;
use Illuminate\Http\Request;
use App\Http\Resources\Set as SetResource;

class SetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gameId = $request->input('game');
        if (isset($gameId)) {
            $sets = Set::orderBy('name')->where('game_id', $gameId)->get();
        } else {
            $sets = Set::orderBy('name')->get();
        }

        $sets->load('cards', 'cards.card');

        return SetResource::collection($sets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $set = Set::create($request->all());

        if (!$set->save()) {
            throw new ApiException(500, 'UNKNOWN_ERROR');
        }

        return response()->json([]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Set $set
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Set $set)
    {
        return new SetResource($set);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Set                 $set
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Set $set)
    {
        $set->fill($request->all());

        if (!$set->save()) {
            throw new ApiException(500, 'UNKNOWN_ERROR');
        }

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Set $set
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Set $set)
    {
        if (!$set->delete()) {
            throw new ApiException(500, 'UNKNOWN_ERROR');
        }

        return response()->json([]);
    }
}
