<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;
use App\Http\Resources\Card as CardResource;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gameId = $request->input('game');
        if (isset($gameId)) {
            $cards = Card::where('game_id', $gameId)->get();
        } else {
            $cards = Card::all();
        }

        $cards->load('cards', 'cards.set',  'cards.rarity', 'cards.language');

        return CardResource::collection($cards);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $card = Card::create($request->all());

        if (!$card->save()) {
            throw new ApiException(500, 'UNKNOWN_ERROR');
        }

        return response()->json([]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Card $card
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        return new CardResource($card);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Card                $card
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        $card->fill($request->all());

        if (!$card->save()) {
            throw new ApiException(500, 'UNKNOWN_ERROR');
        }

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Card $card
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        if (!$card->delete()) {
            throw new ApiException(500, 'UNKNOWN_ERROR');
        }

        return response()->json([]);
    }
}
