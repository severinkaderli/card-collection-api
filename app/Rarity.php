<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rarity extends Model
{
    /**
     * Hidden fields.
     *
     * @var array
     */
    protected $hidden = [
        "created_at",
        "updated_at",
        "game_id"
    ];

    public function game() {
        return $this->belongsTo('App\Game');
    }
}
